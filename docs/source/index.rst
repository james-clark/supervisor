.. Asimov documentation master file, created by
   sphinx-quickstart on Tue Mar 24 12:18:27 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Asimov : The bot framework
==========================

`Asimov` is a python package for producing scripts to automate parameter estimation processes for gravitational wave data on the LIGO Data Grid.

.. warning::
   This documentation is currently being written, so there may be places where it is somewhat defficient.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   readme
   installation
   usage
   api

Pre-built Bot scripts
---------------------

.. toctree::
   :maxdepth: 1
   :caption: Scripts

   olivaw


Module documentation
--------------------

.. toctree::
   :maxdepth: 1
   :caption: Modules

   api/git
   yamlformat
   pipelines
	     
Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
